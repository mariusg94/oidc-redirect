
# OIDC proxy

When using GitLab as a private instance it is not possible to set up an AWS OIDC provider due to the public encryption keys not be accessible to AWS.

This OIDC proxy acts as a middleware between GitLab and the AWS OIDC provider and should be hosted publicly meanwhile it has access to the private gitlab instance.

## How does it work?

First, when setting up an OIDC provider you need to provide an url which hosts a .well-known. 

It is important to understand that OpenID connect (OIDC) is a thin layer on top of OAuth. Normally a client (can be an app, website etc) will import the OIDC settings using a discovery url and use it for user authentication. For the Gitlab SaaS version this is `https://gitlab.com/.well-known/openid-configuration`. This points to things such as `authorization_endpoint`, `token_endpoint` etc.

If we look at this file you will see something similar to

```
{
    "issuer": "https://gitlab.com",
    "authorization_endpoint": "https://gitlab.com/oauth/authorize",
    "token_endpoint": "https://gitlab.com/oauth/token",
    "revocation_endpoint": "https://gitlab.com/oauth/revoke",
    "introspection_endpoint": "https://gitlab.com/oauth/introspect",
    "userinfo_endpoint": "https://gitlab.com/oauth/userinfo",
    "jwks_uri": "https://gitlab.com/oauth/discovery/keys",
    "scopes_supported": [
        "api",
        "read_api",
        "read_user",
    ...
}
```

When a gitlab job is running it is already authenticated, so there is no need to perform the OIDC authentication flow. There is an environment variable at hand called `CI_JOB_JWT` which is a JWT token encrypted with a private key known by gitlab, and can be decrypted by the key(s) found at `jwks_uri`, i.e. for Gitlab SaaS it is `https://gitlab.com/oauth/discovery/keys`.

It looks something like this, and seems to be rotated often (maybe once every hour).

This response has two keys, and which one to use is indicated in a header. However, this code just tries both of them in a loop. Important fields are `kid` which is the key id, `alg` which is `RS256` (= asymetric) but could also have been `HS256` (= symetric). And of course `e` and `n` which together builds up the public key.

```
{
    "keys": [
        {
            "kty": "RSA",
            "kid": "kewiQq9jiC84CvSsJYOB-N6A8WFLSV20Mb-y7IlWDSQ",
            "e": "AQAB",
            "n": "5RyvCSgBoOGNE03CMcJ9Bzo1JDvsU8XgddvRuJtdJAIq5zJ8fiUEGCnMfAZI4of36YXBuBalIycqkgxrRkSOENRUCWN45bf8xsQCcQ8zZxozu0St4w5S-aC7N7UTTarPZTp4BZH8ttUm-VnK4aEdMx9L3Izo0hxaJ135undTuA6gQpK-0nVsm6tRVq4akDe3OhC-7b2h6z7GWJX1SD4sAD3iaq4LZa8y1mvBBz6AIM9co8R-vU1_CduxKQc3KxCnqKALbEKXm0mTGsXha9aNv3pLNRNs_J-cCjBpb1EXAe_7qOURTiIHdv8_sdjcFTJ0OTeLWywuSf7mD0Wpx2LKcD6ImENbyq5IBuR1e2ghnh5Y9H33cuQ0FRni8ikq5W3xP3HSMfwlayhIAJN_WnmbhENRU-m2_hDPiD9JYF2CrQneLkE3kcazSdtarPbg9ZDiydHbKWCV-X7HxxIKEr9N7P1V5HKatF4ZUrG60e3eBnRyccPwmT66i9NYyrcy1_ZNN8D1DY8xh9kflUDy4dSYu4R7AEWxNJWQQov525v0MjD5FNAS03rpk4SuW3Mt7IP73m-_BpmIhW3LZsnmfd8xHRjf0M9veyJD0--ETGmh8t3_CXh3I3R9IbcSEntUl_2lCvc_6B-m8W-t2nZr4wvOq9-iaTQXAn1Au6EaOYWvDRE",
            "use": "sig",
            "alg": "RS256"
        },
        {
            "kty": "RSA",
            "kid": "4i3sFE7sxqNPOT7FdvcGA1ZVGGI_r-tsDXnEuYT4ZqE",
            "e": "AQAB",
            "n": "4cxDjTcJRJFID6UCgepPV45T1XDz_cLXSPgMur00WXB4jJrR9bfnZDx6dWqwps2dCw-lD3Fccj2oItwdRQ99In61l48MgiJaITf5JK2c63halNYiNo22_cyBG__nCkDZTZwEfGdfPRXSOWMg1E0pgGc1PoqwOdHZrQVqTcP3vWJt8bDQSOuoZBHSwVzDSjHPY6LmJMEO42H27t3ZkcYtS5crU8j2Yf-UH5U6rrSEyMdrCpc9IXe9WCmWjz5yOQa0r3U7M5OPEKD1-8wuP6_dPw0DyNO_Ei7UerVtsx5XSTd-Z5ujeB3PFVeAdtGxJ23oRNCq2MCOZBa58EGeRDLR7Q",
            "use": "sig",
            "alg": "RS256"
        }
    ]
}
```

When decoding the encrypted JWT successfully you will get something like the following:

```
{
    "namespace_id": "9001335",
    "namespace_path": "mariusg94",
    "project_id": "51357426",
    "project_path": "mariusg94/oidc-redirect",
    "user_id": "6781335",
    "user_login": "useremail",
    "user_email": "useremail@someone.com",
    "pipeline_id": "1041504522",
    "pipeline_source": "push",
    "job_id": "5323316955",
    "ref": "main",
    "ref_type": "branch",
    "ref_path": "refs/heads/main",
    "ref_protected": "true",
    "jti": "78affc62-fcd7-4671-8de1-ebb50ce38fc4",
    "iss": "gitlab.com",
    "iat": 1697655855,
    "nbf": 1697655850,
    "exp": 1697659455,
    "sub": "job_5323316955"
}
```

Hence when the client wants to ensure that it is gitlab.com that encrypted this, the user will have to use the OIDC configuration to find the decryption key. This makes sure we can trust this data.

When setting up the AWS OIDC provider it is necessary to provide a URL, which is publicly reachable in order to get the keys for decryption. I believe the reason for having two keys is to be able to rotate without any disruptions.

## What does it do?

This will decrypt using the internal gitlab keys and then re-encrypt the data using a new set of keys. It hosts two endpoints; one is the /keys which gives the public keys. And one endpont /transform_token which can be used when assuming a role.

The AWS OIDC provider has to be configured to the OIDC proxy and has to be publicly available.

## How to try it out locally

If you are using VSCode, you can use the extension REST Client to test the server locally using the *.http files. Otherwise curl can be used.

```
python -m pip install -r requirements
python proxy.py
```

## TODO

* Add a .well-known configuration that points to the /keys endpoint
* Add key rotation
* Store keys somewhere so that the server can be restarted without invalidating sessions