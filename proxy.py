from flask import Flask, request, jsonify
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from jwt.algorithms import RSAAlgorithm
from jwt import encode, decode, ExpiredSignatureError, InvalidTokenError
import requests

private_gitlab_url = "https://gitlab.com"
supported_algorithms = ["RS256"]
external_issuer = "external_issuer.com"

app = Flask(__name__)

# Generate RSA key pair
private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
)

# For signing JWT
private_pem = private_key.private_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.TraditionalOpenSSL,
    encryption_algorithm=serialization.NoEncryption(),
)

# For exposing to public
public_pem = private_key.public_key().public_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PublicFormat.SubjectPublicKeyInfo,
)


def fetch_gitlab_public_key(gitlab_url):
    # Fetch the public key from GitLab's JWKS endpoint
    response = requests.get(f"{gitlab_url}/oauth/discovery/keys")
    jwks = response.json()
    return jwks["keys"]


@app.route("/keys", methods=["GET"])
def keys():
    # Expose public key
    return jsonify(
        {
            "keys": [
                {
                    "alg": "RS256",
                    "kty": "RSA",
                    "use": "sig",
                    "key": public_pem.decode("utf-8"),
                }
            ]
        }
    )


def decode_original_jwk(keys, original_token):
    for kid in range(len(keys)):
        try:
            rsa_key = RSAAlgorithm.from_jwk(keys[kid])
            alg = keys[kid]["alg"]
            decoded_token = decode(
                original_token,
                rsa_key,
                algorithms=[alg],
            )
            print(f"found that key id {kid} worked!")
            return decoded_token, alg
        except:
            pass
    return None


@app.route("/transform_token", methods=["POST"])
def transform_token():
    # Get the original token from the incoming request
    original_token = request.json.get("token")
    if not original_token:
        return jsonify({"error": "No token found in request"}), 401

    # Change the issuer and encode a new token
    try:
        keys = fetch_gitlab_public_key(private_gitlab_url)
        if not keys:
            return jsonify({"error": "No keys found at private gitlab"}), 401

        decoded_token, alg = decode_original_jwk(keys, original_token)
        print(decoded_token)
        if not decoded_token:
            return jsonify({"error": "No valid keys found at private gitlab"}), 401

        if not alg in supported_algorithms:
            return (
                jsonify(
                    {
                        "error": f"Only the following algortihms are supported: {supported_algorithms}"
                    }
                ),
                401,
            )

        decoded_token["extra"] = external_issuer
        new_token = encode(decoded_token, private_pem, algorithm=alg)

        return jsonify({"new_token": new_token})

    except ExpiredSignatureError:
        return jsonify({"error": "Expired token"}), 401

    except InvalidTokenError:
        return jsonify({"error": "Invalid token"}), 401


if __name__ == "__main__":
    app.run(debug=True)
