from python:3.11-slim

COPY . /app
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 8000

ENTRYPOINT gunicorn --bind 0.0.0.0:8000 proxy:app
